'use strict';


/*===== Sidebar Navigation Start =====*/
document.addEventListener('DOMContentLoaded', () => {
    const linkItems = document.querySelectorAll('.side-nav__item');
    let activeLink = document.querySelector('.side-nav__item:first-child');

    // Function to activate a link item
    const activateLink = linkItem => {
        activeLink.classList.remove('side-nav__item--active');
        linkItem.classList.add('side-nav__item--active');
        activeLink = linkItem;
    };

    // Set the default active link (Hotel)
    activateLink(activeLink);

    // Add click event listeners using a for loop
    for (let i = 0; i < linkItems.length; i++) {
        linkItems[i].addEventListener('click', e => {
            e.preventDefault(); // Prevent the default link behavior
            activateLink(linkItems[i]); // Activate the clicked link item
        });
    }
});
/*===== Sidebar Navigation End   =====*/